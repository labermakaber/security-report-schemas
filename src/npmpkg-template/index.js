// ES6 Module entrypoint
import dastSchema from './dist/dast-report-format.json';
import sastSchema from './dist/sast-report-format.json';
import dependencyScanningSchema from './dist/dependency-scanning-report-format.json';
import secretDetectionSchema from './dist/secret-detection-report-format.json';
import clusterImageScanningSchema from './dist/cluster-image-scanning-report-format.json';
import containerScanningSchema from './dist/container-scanning-report-format.json';
import coverageFuzzingSchema from './dist/coverage-fuzzing-report-format.json';

export const DastReportSchema = dastSchema;
export const SastReportSchema = sastSchema;
export const DependencyScanningReportSchema = dependencyScanningSchema;
export const SecretDetectionReportSchema = secretDetectionSchema;
export const ClusterImageScanningReportSchema = clusterImageScanningSchema;
export const ContainerScanningReportSchema = containerScanningSchema;
export const CoverageFuzzingReportSchema = coverageFuzzingSchema;

export const schemas = {
    dast: DastReportSchema,
    sast: SastReportSchema,
    dependency_scanning: DependencyScanningReportSchema,
    secret_detection: SecretDetectionReportSchema,
    cluster_image_scanning: ClusterImageScanningReportSchema,
    container_scanning: ContainerScanningReportSchema,
    coverage_fuzzing: CoverageFuzzingReportSchema,
};

export default schemas;
