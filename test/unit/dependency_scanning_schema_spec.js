import b from './builders/index'
import {schemas} from './support/schemas'

describe('dependency scanning schema', () => {

  it('should validate location', () => {
    const report = b.dependency_scanning.report({
      vulnerabilities: [b.dependency_scanning.vulnerability({
        location: {
          file: 'Gemfile.lock',
          dependency: {
            package: {name: 'rack'},
            version: '2.0.4',
            direct: true
          }
        },
      })]
    })

    expect(schemas.dependency_scanning.validate(report).success).toBeTruthy()
  })

  it('location file is required', () => {
    const report = b.dependency_scanning.report({
      vulnerabilities: [b.dependency_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'rack'},
            version: '2.0.4'
          }
        }
      })]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'file\'')
  })

  it('location dependency is required', () => {
    const report = b.dependency_scanning.report({
      vulnerabilities: [b.dependency_scanning.vulnerability({
        location: {
          file: 'Gemfile.lock'
        }
      })]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'dependency\'')
  })

  it('location dependency.package is required', () => {
    const report = b.dependency_scanning.report({
      vulnerabilities: [b.dependency_scanning.vulnerability({
        location: {
          file: 'Gemfile.lock',
          dependency: {
            version: '1.0.6-8.1'
          }
        }
      })]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'package\'')
  })

  it('location dependency.package.name is required', () => {
    const report = b.dependency_scanning.report({
      vulnerabilities: [b.dependency_scanning.vulnerability({
        location: {
          file: 'Gemfile.lock',
          dependency: {
            package: {noname: 'bzip2'},
            version: '1.0.6-8.1'
          }
        }
      })]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'name\'')
  })

  it('location dependency.package.version is required', () => {
    const report = b.dependency_scanning.report({
      vulnerabilities: [b.dependency_scanning.vulnerability({
        location: {
          file: 'Gemfile.lock',
          dependency: {
            package: {name: 'bzip2'},
          }
        }
      })]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'version\'')
  })

})
