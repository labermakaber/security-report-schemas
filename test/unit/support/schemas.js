import Ajv from "ajv";
import * as dastSchema from '../../../dist/dast-report-format.json'
import * as sastSchema from '../../../dist/sast-report-format.json'
import * as dependencyScanningSchema from '../../../dist/dependency-scanning-report-format.json'
import * as secretDetectionSchema from '../../../dist/secret-detection-report-format.json'
import * as clusterImageScanningSchema from '../../../dist/cluster-image-scanning-report-format.json'
import * as containerScanningSchema from '../../../dist/container-scanning-report-format.json'
import * as coverageFuzzingSchema from '../../../dist/coverage-fuzzing-report-format.json'
import * as jsonDraft07Specification from './json-schema-draft-07.json'

const validateSchema = (schema) => {
  return (report) => {
    let ajv = new Ajv({strict: true, allowUnionTypes: true, allErrors: true});
    ajv.addKeyword("self")

    const addFormats = require("ajv-formats")
    addFormats(ajv)

    const valid = ajv.validate(schema, report)

    return {
      success: valid,
      errors: ajv.errors,
    }
  }
}

export const schemas = {
  dast: {validate: validateSchema(dastSchema)},
  sast: {validate: validateSchema(sastSchema)},
  dependency_scanning: {validate: validateSchema(dependencyScanningSchema)},
  secret_detection: {validate: validateSchema(secretDetectionSchema)},
  cluster_image_scanning: {validate: validateSchema(clusterImageScanningSchema)},
  container_scanning: {validate: validateSchema(containerScanningSchema)},
  coverage_fuzzing: {validate: validateSchema(coverageFuzzingSchema)},
  json_draft_07: {validate: validateSchema(jsonDraft07Specification)},
}
