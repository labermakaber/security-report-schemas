#!/bin/bash

status=0

bash_unit test/test-cluster-image-scanning.sh \
          test/test-container-scanning.sh \
          test/test-sast.sh \
          test/test-coverage-fuzzing.sh \
          test/test-dependency-scanning.sh \
          test/test-dast.sh \
          test/test-secret-detection.sh || status=1

bash_unit test/test-release-utils.sh || status=1

echo
echo "Running Javascript specs..."
npx jasmine --require=esm --config=test/unit/support/jasmine.json || status=1

exit "$status"
